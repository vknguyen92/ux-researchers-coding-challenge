//
//  PostCountAndLikeDownloader.h
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/7/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostCountAndLikeDownloader : NSObject
+ (id)sharedManager;
- (void)fetchPostLikeAndCommentCountFromPostId: (NSString *)postId completion: (void (^)(NSInteger likeCount, NSInteger commentCount, NSError *error))completion;
@end
