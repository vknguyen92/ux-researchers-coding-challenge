//
//  UserModel.h
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/6/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DB_UserModel.h"

@interface UserModel : NSObject
@property (nonatomic, retain) NSString *idString;
@property (nonatomic, retain) NSString *fullName;
@property (nonatomic, retain) NSString *phoneNumber;
@property (nonatomic, retain) NSString *dobString;
@property (nonatomic, retain) NSString *imgUrl;

+ (UserModel *)addUserWithIdString: (NSString *)idString fullName: (NSString *)fullName phoneNumber: (NSString *)phoneNumber dobString: (NSString *)dobString imgUrl: (NSString *)imgUrl;

+ (UserModel *)fromDbUser: (DB_UserModel *)dbUser;
- (NSString *)imagePath;
@end
