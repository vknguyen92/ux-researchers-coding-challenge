//
//  TableViewCell.h
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/7/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString * const kTableViewCellIdentifier = @"kTableViewCellIdentifier";

@interface TableViewCell : UITableViewCell
- (void)configureCellWithUserImage: (UIImage *)userImage userName: (NSString *)userName postTimeAgo: (NSString *)postTimeAgo postImage: (UIImage *)postImage postMessage: (NSString *)postMessage likeCount: (NSInteger)likeCount postCount: (NSInteger)postCount;
@end
