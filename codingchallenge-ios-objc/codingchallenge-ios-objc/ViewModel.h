//
//  ViewModel.h
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/7/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SocialDataManager.h"
#import <UIKit/UIKit.h>
#import "PostModel.h"
#import "UserModel.h"

@interface ViewModel : NSObject
@property (readonly, nonatomic, assign) BOOL fetchCompleted;
@property (readonly, nonatomic, assign) BOOL imageDownloaded;
@property (readonly, nonatomic, assign) BOOL postLikeAndCommentCountFetched;
- (void)fetchData;
- (NSArray *)getPosts;
- (UIImage *)getImageFromPostModel: (PostModel *)postModel;
- (UIImage *)getImageFromUserModel: (UserModel *)userModel;
- (NSString *)convertToStringAgoFromDateString: (NSString *)dateString;
@end
