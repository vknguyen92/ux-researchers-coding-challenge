//
//  ImageDownloadAndCache.h
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/6/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImageDownloadOperation : NSObject<NSURLSessionDownloadDelegate>
@property (readonly, nonatomic, strong) NSURL *tempDownloadedFileLocation;

- (instancetype)initWithUrl: (NSString *)url progressBlock: (void (^)(CGFloat progress))progressBlock completionBlock:(void (^)(ImageDownloadOperation *ImageDownloadOperation))completionBlock;
- (void)startDownload;
@end
