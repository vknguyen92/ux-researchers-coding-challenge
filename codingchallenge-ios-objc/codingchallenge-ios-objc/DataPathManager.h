//
//  DataPathManager.h
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/6/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataPathManager : NSObject
+ (NSString *)socialFolder;
+ (NSString *)postImageFolder;
+ (NSString *)userImageFolder;
@end
