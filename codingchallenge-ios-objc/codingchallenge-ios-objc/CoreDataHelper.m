//
//  CoreDataHelper.m
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/6/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "CoreDataHelper.h"
#import "DataPathManager.h"

static NSString * const kPostModelEntityName = @"Post";
static NSString * const kUserModelEntityName = @"User";

@interface CoreDataHelper()
{
    NSManagedObjectModel *managedObjectModel;
    NSPersistentStoreCoordinator *persistentStoreCoordinator;
    NSManagedObjectContext *managedObjectContext;
}

@end

@implementation CoreDataHelper
static CoreDataHelper *sharedInstace = nil;

static bool isFirstAccess = YES;

+ (id)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        isFirstAccess = NO;
        sharedInstace = [[self alloc] init];
        [sharedInstace createSocialFolder];
        [sharedInstace createManagedObjectContext];
    });
    
    return sharedInstace;
}

#pragma mark - setup Core Data
- (NSManagedObjectModel *)managedObjectModel
{
    if (managedObjectModel)
    {
        return managedObjectModel;
    }
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Social" withExtension:@"momd"];
    managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return managedObjectModel;
}

- (BOOL)addPersistentStore:(NSError **)errorPtr
{
    NSURL *url = [NSURL fileURLWithPath:[self socialDBFile]];
    NSPersistentStore *result = [persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                                         configuration:nil
                                                                                   URL:url
                                                                               options:nil
                                                                                 error:errorPtr];
    return (result != nil);
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (persistentStoreCoordinator)
    {
        return persistentStoreCoordinator;
    }
    
    NSManagedObjectModel *mom = [self managedObjectModel];
    if (mom == nil)
    {
        NSLog(@"%@: No model to generate a store from", [self class]);
        return nil;
    }
    
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:mom];
    
    NSError *error = nil;
    if ([self addPersistentStore:&error] == NO)
    {
        NSLog(@"%@: Error creating persistent store: %@", [self class], error);
        persistentStoreCoordinator = nil;
    }
    
    return persistentStoreCoordinator;
}

- (void)createManagedObjectContext
{
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator)
    {
        if (managedObjectContext == nil)
        {
            managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
            [managedObjectContext setPersistentStoreCoordinator:coordinator];
            [managedObjectContext setMergePolicy:NSOverwriteMergePolicy];
        }
    }
}

- (NSString *)socialDBFile
{
    return [[DataPathManager socialFolder] stringByAppendingPathComponent:@"Social.sqlite"];
}

- (void)createSocialFolder
{
    [self createFolder:[DataPathManager postImageFolder]];
    [self createFolder:[DataPathManager userImageFolder]];
}

- (void)createFolder: (NSString *)folderPath
{
    NSError *error = nil;
    
    BOOL isOK = [[NSFileManager defaultManager] createDirectoryAtPath:folderPath
                                          withIntermediateDirectories:YES
                                                           attributes:nil
                                                                error:&error];
    if (!isOK)
    {
        NSLog(@"%@: Unable to create logDirectory(%@) due to error: %@",
              [self class], folderPath, error);
    }
}

#pragma mark - save and load posts + users
- (NSArray *)fetchPosts
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:kPostModelEntityName];
    NSArray *postModels = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    return postModels;
}

- (NSArray *)fetchUsers
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:kUserModelEntityName];
    NSArray *userModels = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    return userModels;
}

- (DB_PostModel *)savePost: (PostModel *)postModel
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:kPostModelEntityName];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idString LIKE %@", [postModel idString]];
    [fetchRequest setPredicate:predicate];
    DB_PostModel *savedPostModel = [[[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy] firstObject];

    if (savedPostModel == nil) {
        savedPostModel = [NSEntityDescription insertNewObjectForEntityForName:kPostModelEntityName inManagedObjectContext:managedObjectContext];
    }
    
    [savedPostModel setIdString:[postModel idString]];
    [savedPostModel setMessage:[postModel message]];
    [savedPostModel setImgUrl:[postModel imgUrl]];
    [savedPostModel setPostedAtString:[postModel postedAtString]];
    [savedPostModel setUser:[self saveUser:[postModel user]]];
    
    NSError *error = nil;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        return nil;
    }
    
    return savedPostModel;
}

- (DB_UserModel *)saveUser: (UserModel *)userModel
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:kUserModelEntityName];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idString LIKE %@", [userModel idString]];
    [fetchRequest setPredicate:predicate];
    DB_UserModel *savedUserModel = [[[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy] firstObject];
    
    if (savedUserModel == nil) {
        savedUserModel = [NSEntityDescription insertNewObjectForEntityForName:kUserModelEntityName inManagedObjectContext:managedObjectContext];
    }
    
    [savedUserModel setIdString:[userModel idString]];
    [savedUserModel setFullName:[userModel fullName]];
    [savedUserModel setDobString:[userModel dobString]];
    [savedUserModel setPhoneNumber:[userModel phoneNumber]];
    [savedUserModel setImgUrl:[userModel imgUrl]];
    
    NSError *error = nil;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        return nil;
    }
    
    return savedUserModel;
}

- (void)updateForPostId: (NSString *)postId likeCount: (NSInteger)likeCount andCommentCount: (NSInteger)commentCount
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:kPostModelEntityName];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idString LIKE %@", postId];
    [fetchRequest setPredicate:predicate];
    DB_PostModel *savedPostModel = [[[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy] firstObject];
    
    [savedPostModel setLikeCount:@(likeCount)];
    [savedPostModel setCommentCount:@(commentCount)];
    
    NSError *error = nil;
    if (![managedObjectContext save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
}
@end
