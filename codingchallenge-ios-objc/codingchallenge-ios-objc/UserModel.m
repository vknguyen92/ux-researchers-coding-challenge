//
//  UserModel.m
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/6/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "UserModel.h"
#import "DataPathManager.h"

@implementation UserModel
- (instancetype)initWithIdString: (NSString *)idString fullName: (NSString *)fullName phoneNumber: (NSString *)phoneNumber dobString: (NSString *)dobString imgUrl: (NSString *)imgUrl
{
    self = [super init];
    if (self) {
        _idString = idString;
        _fullName = fullName;
        _phoneNumber = phoneNumber;
        _dobString = dobString;
        _imgUrl = imgUrl;
    }
    return self;
}

+ (UserModel *)addUserWithIdString: (NSString *)idString fullName: (NSString *)fullName phoneNumber: (NSString *)phoneNumber dobString: (NSString *)dobString imgUrl: (NSString *)imgUrl
{
    UserModel *userModel = [[UserModel alloc] initWithIdString:idString fullName:fullName phoneNumber:phoneNumber dobString:dobString imgUrl:imgUrl];
    return userModel;
}

+ (UserModel *)fromDbUser: (DB_UserModel *)dbUser
{
    UserModel *userModel = [[UserModel alloc] initWithIdString:[dbUser idString] fullName:[dbUser fullName] phoneNumber:[dbUser phoneNumber] dobString:[dbUser dobString] imgUrl:[dbUser imgUrl]];
    return userModel;
}

- (NSString *)imagePath
{
    NSString *escapedUrlString = [[self imgUrl] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *imageName = [NSString stringWithFormat:@"%@_%@", [self idString], escapedUrlString];
    return [[DataPathManager userImageFolder] stringByAppendingPathComponent:imageName];
}
@end
