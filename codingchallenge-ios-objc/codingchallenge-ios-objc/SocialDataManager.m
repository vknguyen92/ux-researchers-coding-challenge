//
//  SocialDataManager.m
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/7/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SocialDataManager.h"
#import "BackendPullManager.h"
#import "ImageDownloader.h"
#import "DataPathManager.h"
#import "CoreDataHelper.h"
#import "PostCountAndLikeDownloader.h"

@interface SocialDataManager()
@property (nonatomic, strong) NSArray *postModelsArray;
@end

@implementation SocialDataManager
+ (id)sharedManager {
    static SocialDataManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
        [sharedManager setPostModelsArray:[[NSArray alloc] init]];
    });
    return sharedManager;
}

- (void)fetchPostsFromServer
{
    NSLog(@"Fetch posts from server");
    [[BackendPullManager sharedManager] fetchPostsCompletion:^(NSArray *postModels, NSError *error) {
        
        NSLog(@"Fetch posts from server completed: %lu posts", (unsigned long)[postModels count]);
        
        if (postModels != nil && [postModels count] > 0) {
            [self setPostModelsArray:postModels];
        }
        
        // post notification to notify post list has been fetched
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPostsFetched object:nil userInfo:nil];
        });
        
        for (PostModel *postModel in postModels) {
            // save to db
            [[CoreDataHelper sharedInstance] savePost:postModel];
            
            // fetch like and cmt count
            [self fetchPostLikeAndCommentCountForPost:postModel];
            
            // download and save post and user images to local path
            NSString *postImagePath = [postModel imagePath];
            [self downloadImageAndSaveToLocalFileWithImageUrl:[postModel imgUrl] andPath:postImagePath];
            
            NSString *userImagePath = [[postModel user] imagePath];
            [self downloadImageAndSaveToLocalFileWithImageUrl:[[postModel user] imgUrl] andPath:userImagePath];
        }
    }];
}

- (void)fetchPostLikeAndCommentCountForPost: (PostModel *)postModel
{
    NSLog(@"Fetch posts' likes and comment count from server");
    [[PostCountAndLikeDownloader sharedManager] fetchPostLikeAndCommentCountFromPostId:[postModel idString] completion:^(NSInteger likeCount, NSInteger commentCount, NSError *error) {

        NSLog(@"Fetch posts' likes and comment count from server completed. %@ likes - %@ comments", @(likeCount), @(commentCount));
        
        if (error == nil) {
            [[CoreDataHelper sharedInstance] updateForPostId:[postModel idString] likeCount:likeCount andCommentCount:commentCount];
            [postModel setLikeCount:@(likeCount)];
            [postModel setCommentCount:@(commentCount)];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPostCountAndCommentFetched object:nil userInfo:nil];
            });
        }
    }];
}

- (void)downloadImageAndSaveToLocalFileWithImageUrl: (NSString *)url andPath: (NSString *)path
{
    [[ImageDownloader sharedManager] downloadImageFromUrl:url saveToPath:path completion:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationImageDownloaded object:nil userInfo:nil];
        });
    }];
}

- (void)fetchPostsFromLocal
{
    NSLog(@"Fetch posts from local");
    NSArray *dbPostsArray = [[CoreDataHelper sharedInstance] fetchPosts];
    NSMutableArray *postModelsArray = [[NSMutableArray alloc] init];
    for (DB_PostModel *dbPost in dbPostsArray) {
        PostModel *postModel = [PostModel fromDbPost:dbPost];
        [postModelsArray addObject:postModel];
        [self checkIfImageDownloadedAndDownloadIfNeededFromPost:postModel];
    }
    [self setPostModelsArray:postModelsArray];

    NSLog(@"Fetch posts from local completed: %lu posts", (unsigned long)[postModelsArray count]);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPostsFetched object:nil userInfo:nil];
    });
}

- (void)checkIfImageDownloadedAndDownloadIfNeededFromPost: (PostModel *)postModel
{
    NSString *postImagePath = [postModel imagePath];
    NSString *userImagePath = [[postModel user] imagePath];
    
    if ([self checkIfImageExistInPath:postImagePath] == false){
        [self downloadImageAndSaveToLocalFileWithImageUrl:[postModel imgUrl] andPath:postImagePath];
    }
    if ([self checkIfImageExistInPath:userImagePath] == false){
        [self downloadImageAndSaveToLocalFileWithImageUrl:[[postModel user] imgUrl] andPath:postImagePath];
    }
}

- (BOOL)checkIfImageExistInPath: (NSString *)path
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    return [fileManager fileExistsAtPath:path];
}

- (NSDate *)convertToDateFromString: (NSString *)dateString
{
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    NSDate *date = [dateFormat dateFromString:dateString];

    return date;
}
@end
