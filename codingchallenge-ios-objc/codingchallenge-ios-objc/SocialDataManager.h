//
//  SocialDataManager.h
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/7/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString * const kNotificationPostsFetched = @"kNotificationPostsFetched";
static NSString * const kNotificationImageDownloaded = @"kNotificationImageDownloaded";
static NSString * const kNotificationPostCountAndCommentFetched = @"kNotificationPostCountAndCommentFetched";

@interface SocialDataManager : NSObject
@property (readonly, nonatomic, strong) NSArray *postModelsArray;
+ (id)sharedManager;
- (void)fetchPostsFromServer;
- (void)fetchPostsFromLocal;
- (NSDate *)convertToDateFromString: (NSString *)dateString;
@end
