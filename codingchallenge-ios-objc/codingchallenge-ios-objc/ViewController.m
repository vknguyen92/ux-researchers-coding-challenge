//
//  ViewController.m
//  codingchallenge-ios-objc
//
//  Created by Tang Han on 19/2/16.
//  Copyright © 2016 User Experience Research. All rights reserved.
//

#import "ViewController.h"
#import "ViewModel.h"
#import "TableViewCell.h"
#import "PostModel.h"
#import "UserModel.h"

@interface ViewController ()
@property (nonatomic, strong) ViewModel *viewModel;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViews];
    [self setViewModel:[[ViewModel alloc] init]];
    [self setupObservers];
    [[self viewModel] fetchData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupNavigation];
}

- (void)setupNavigation
{
    [[self navigationItem] setTitle:@"Moments"];

    UINavigationBar *bar = [self.navigationController navigationBar];
    [bar setBarTintColor:[UIColor lightGrayColor]];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    [self setupCameraButton];
}

- (void)setupCameraButton
{
    UIImage* image = [UIImage imageNamed:@"icon_moments_camera"];
    CGRect frameimg = CGRectMake(0, 0, 20/image.size.height*image.size.width, 20);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image forState:UIControlStateNormal];
    [someButton addTarget:self action:@selector(cameraButtonPressed:)
         forControlEvents:UIControlEventTouchUpInside];
    [someButton setShowsTouchWhenHighlighted:YES];
    
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.rightBarButtonItem = mailbutton;
}

- (void)cameraButtonPressed:(id)sender
{
    // nothing to do here
}

- (void)setupViews
{
    [[self view] setBackgroundColor:[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1]];
    [[self tableView] setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [[self tableView] setShowsVerticalScrollIndicator:NO];
    [[self tableView] registerClass:[TableViewCell class] forCellReuseIdentifier:kTableViewCellIdentifier];
}

- (void)setupObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notificationAppEnterForeground:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:[UIApplication sharedApplication]];
    
    [[self viewModel] addObserver:self forKeyPath:@"fetchCompleted" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
    [[self viewModel] addObserver:self forKeyPath:@"imageDownloaded" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
    [[self viewModel] addObserver:self forKeyPath:@"postLikeAndCommentCountFetched" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
}

- (void)notificationAppEnterForeground: (NSNotification *)noti
{
    [[self viewModel] fetchData];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"fetchCompleted"]) {
        [[self tableView] reloadData];
    } else if ([keyPath isEqualToString:@"imageDownloaded"] || [keyPath isEqualToString:@"postLikeAndCommentCountFetched"]) {
        NSArray *visibleCells = [[self tableView] visibleCells];
        for (TableViewCell *cell in visibleCells) {
            NSIndexPath *indexPath = [[self tableView] indexPathForCell:cell];
            [self configureCell:cell atRow:[indexPath row]];
        }
    }
}

#pragma mark - tableView delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[self viewModel] getPosts] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellIdentifier forIndexPath:indexPath];
    [self configureCell:cell atRow:[indexPath row]];
    return cell;
}

- (void)configureCell: (TableViewCell *)cell atRow: (NSInteger)row
{
    PostModel *postModel = [[[self viewModel] getPosts] objectAtIndex:row];
    if (postModel != nil) {
        NSString *timeAgo = [[self viewModel] convertToStringAgoFromDateString:[postModel postedAtString]];
        [cell configureCellWithUserImage:[[self viewModel] getImageFromUserModel:[postModel user]]
                                userName:[[postModel user] fullName]
                             postTimeAgo:timeAgo
                               postImage:[[self viewModel] getImageFromPostModel:postModel]
                             postMessage:[postModel message]
                               likeCount:[[postModel likeCount] integerValue]
                               postCount:[[postModel commentCount] integerValue]];
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

@end
