//
//  ApiClient.h
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/6/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApiClient : NSObject
+ (id)sharedManager;
- (void)getPostsCompletion: (void (^)(NSData *data, NSError *error))completion;
- (void)getLikeCountFromPostId: (NSString *)postId completion: (void (^)(NSData *data, NSError *error))completion;
- (void)getCommentCountFromPostId: (NSString *)postId completion: (void (^)(NSData *data, NSError *error))completion;
@end
