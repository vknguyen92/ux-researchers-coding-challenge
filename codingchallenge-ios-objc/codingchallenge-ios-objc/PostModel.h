//
//  PostModel.h
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/6/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserModel.h"
#import "DB_PostModel.h"

@interface PostModel : NSObject
@property (nonatomic, retain) NSString *idString;
@property (nonatomic, retain) NSString *message;
@property (nonatomic, retain) NSString *postedAtString;
@property (nonatomic, retain) NSString *imgUrl;
@property (nonatomic, retain) NSNumber *likeCount;
@property (nonatomic, retain) NSNumber *commentCount;
@property (nonatomic, retain) UserModel *user;

+ (PostModel *)addPostWithIdString: (NSString *)idString message: (NSString *)message postedAtString: (NSString *)postedAtString imgUrl: (NSString *)imgUrl likeCount: (NSNumber *)likeCount commentCount: (NSNumber *)commentCount user: (UserModel *)user;

+ (PostModel *)fromDbPost: (DB_PostModel *)dbPost;
- (NSString *)imagePath;
@end
