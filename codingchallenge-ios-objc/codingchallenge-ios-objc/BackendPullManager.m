//
//  BackendPullManager.m
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/5/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "BackendPullManager.h"
#import "ApiClient.h"
#import "JSONParser.h"

@interface BackendPullManager()

@end

@implementation BackendPullManager

+ (id)sharedManager {
    static BackendPullManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

- (void)fetchPostsCompletion: (void (^)(NSArray *postModels, NSError *error))completion
{
    [[ApiClient sharedManager] getPostsCompletion:^(NSData *data, NSError *error) {
        if (error == nil) {
            NSArray *postModelsArray = [[JSONParser sharedManager] parsePostJsonFromData:data];
            completion(postModelsArray, error);
        } else {
            completion(nil, error);
        }
    }];
}

- (void)getLikeCountFromPostId: (NSString *)postId completion: (void (^)(NSInteger likeCount, NSError *error))completion
{
    [[ApiClient sharedManager] getLikeCountFromPostId:postId completion:^(NSData *data, NSError *error) {
        NSInteger likeCount = [[JSONParser sharedManager] parseCountFromData:data];
        completion(likeCount, error);
    }];
}

- (void)getCommentCountFromPostId: (NSString *)postId completion: (void (^)(NSInteger commentCount, NSError *error))completion
{
    [[ApiClient sharedManager] getCommentCountFromPostId:postId completion:^(NSData *data, NSError *error) {
        NSInteger likeCount = [[JSONParser sharedManager] parseCountFromData:data];
        completion(likeCount, error);
    }];
}


@end
