//
//  PostCountAndLikeDownloader.m
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/7/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "PostCountAndLikeDownloader.h"
#import "BackendPullManager.h"

@interface PostCountAndLikeDownloader()
@property (nonatomic, strong) NSOperationQueue *postLikeAndCommentDownloadQueue;
@end

@implementation PostCountAndLikeDownloader
+ (id)sharedManager {
    static PostCountAndLikeDownloader *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
        [sharedManager setPostLikeAndCommentDownloadQueue:[[NSOperationQueue alloc] init]];
        [[sharedManager postLikeAndCommentDownloadQueue] setMaxConcurrentOperationCount:1];
    });
    return sharedManager;
}

- (void)fetchPostLikeAndCommentCountFromPostId: (NSString *)postId completion: (void (^)(NSInteger likeCount, NSInteger commentCount, NSError *error))completion
{
    NSBlockOperation *operation = [[NSBlockOperation alloc] init];
    
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    [operation addExecutionBlock:^{
        [[BackendPullManager sharedManager] getLikeCountFromPostId:postId completion:^(NSInteger likeCount, NSError *error) {
            
            [[BackendPullManager sharedManager] getCommentCountFromPostId:postId completion:^(NSInteger commentCount, NSError *error) {
                completion(likeCount, commentCount, error);
                dispatch_semaphore_signal(semaphore);
            }];
            
        }];
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    }];
    
    [[self postLikeAndCommentDownloadQueue] addOperation:operation];
}

@end
