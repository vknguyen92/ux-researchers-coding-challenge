//
//  BackendPullManager.h
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/5/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BackendPullManager : NSObject
+ (id)sharedManager;
- (void)fetchPostsCompletion: (void (^)(NSArray *postModels, NSError *error))completion;
- (void)getLikeCountFromPostId: (NSString *)postId completion: (void (^)(NSInteger likeCount, NSError *error))completion;
- (void)getCommentCountFromPostId: (NSString *)postId completion: (void (^)(NSInteger commentCount, NSError *error))completion;
@end
