//
//  CoreDataHelper.h
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/6/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "PostModel.h"
#import "UserModel.h"
#import "DB_PostModel.h"
#import "DB_UserModel.h"

@interface CoreDataHelper : NSObject
+ (id)sharedInstance;
- (NSArray *)fetchPosts;
- (NSArray *)fetchUsers;
- (DB_PostModel *)savePost: (PostModel *)postModel;
- (DB_UserModel *)saveUser: (UserModel *)userModel;
- (void)updateForPostId: (NSString *)postId likeCount: (NSInteger)likeCount andCommentCount: (NSInteger)commentCount;
@end
