//
//  TableViewCell.m
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/7/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "TableViewCell.h"

@interface TableViewCell()
@property (nonatomic, strong) UIView *customContentView;

@property (nonatomic, strong) UIImageView *userImageView;
@property (nonatomic, strong) UILabel *userNameLabel;
@property (nonatomic, strong) UILabel *postTimeAgoLabel;
@property (nonatomic, strong) UIImageView *postImageView;
@property (nonatomic, strong) UILabel *postMessageLabel;
@property (nonatomic, strong) UIImageView *likeCountImageView;
@property (nonatomic, strong) UILabel *likeCountLabel;
@property (nonatomic, strong) UIImageView *postCountImageView;
@property (nonatomic, strong) UILabel *postCountLabel;
@end

@implementation TableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupViews];
        [self setupLayout];
    }
    return self;
}

- (void)setupViews
{
    [self setBackgroundColor:[UIColor clearColor]];
    
    [self setCustomContentView:[[UIView alloc] init]];
    [self addSubview:[self customContentView]];
    [[self customContentView] setBackgroundColor:[UIColor whiteColor]];
    
    [self setUserImageView:[[UIImageView alloc] init]];
    [self setPostImageView:[[UIImageView alloc] init]];
    [self setUserNameLabel:[[UILabel alloc] init]];
    [self setPostTimeAgoLabel:[[UILabel alloc] init]];
    [self setPostMessageLabel:[[UILabel alloc] init]];
    [self setLikeCountLabel:[[UILabel alloc] init]];
    [self setPostCountLabel:[[UILabel alloc] init]];
    [self setLikeCountImageView:[[UIImageView alloc] init]];
    [self setPostCountImageView:[[UIImageView alloc] init]];
    
    [[[self userImageView] layer] setCornerRadius:25];
    [[[self userImageView] layer] setMasksToBounds:YES];
    [[self userImageView] setContentMode:UIViewContentModeScaleAspectFit];
    [[self postImageView] setContentMode:UIViewContentModeScaleAspectFill];
    [[self postImageView] setClipsToBounds:YES];
    [[self likeCountImageView] setContentMode:UIViewContentModeScaleAspectFit];
    [[self likeCountImageView] setImage:[UIImage imageNamed:@"icon_moments_like_black"]];
    [[self postCountImageView] setContentMode:UIViewContentModeScaleAspectFit];
    [[self postCountImageView] setImage:[UIImage imageNamed:@"icon_moments_comment"]];
    [[self postMessageLabel] setNumberOfLines:0];
    
    [[self customContentView] addSubview:[self userImageView]];
    [[self customContentView] addSubview:[self userNameLabel]];
    [[self customContentView] addSubview:[self postTimeAgoLabel]];
    [[self customContentView] addSubview:[self postImageView]];
    [[self customContentView] addSubview:[self postMessageLabel]];
    [[self customContentView] addSubview:[self likeCountLabel]];
    [[self customContentView] addSubview:[self postCountLabel]];
    [[self customContentView] addSubview:[self likeCountImageView]];
    [[self customContentView] addSubview:[self postCountImageView]];
}

- (void)setupLayout
{
    [[self customContentView] setTranslatesAutoresizingMaskIntoConstraints: NO];
    [[self userImageView] setTranslatesAutoresizingMaskIntoConstraints: NO];
    [[self userNameLabel] setTranslatesAutoresizingMaskIntoConstraints: NO];
    [[self postTimeAgoLabel] setTranslatesAutoresizingMaskIntoConstraints: NO];
    [[self postImageView] setTranslatesAutoresizingMaskIntoConstraints: NO];
    [[self postMessageLabel] setTranslatesAutoresizingMaskIntoConstraints: NO];
    [[self likeCountLabel] setTranslatesAutoresizingMaskIntoConstraints: NO];
    [[self postCountLabel] setTranslatesAutoresizingMaskIntoConstraints: NO];
    [[self likeCountImageView] setTranslatesAutoresizingMaskIntoConstraints: NO];
    [[self postCountImageView] setTranslatesAutoresizingMaskIntoConstraints: NO];
    
    CGFloat imgHeight = [UIScreen mainScreen].bounds.size.height / 4.2;
    
    NSArray<__kindof NSLayoutConstraint *> *constraints =
    @[
      [NSLayoutConstraint constraintWithItem:[self customContentView] attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1 constant:10],
      [NSLayoutConstraint constraintWithItem:[self customContentView] attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1 constant:-10],
      [NSLayoutConstraint constraintWithItem:[self customContentView] attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:10],
      [NSLayoutConstraint constraintWithItem:[self customContentView] attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:-10],
      
      [NSLayoutConstraint constraintWithItem:[self userImageView] attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:[self customContentView] attribute:NSLayoutAttributeLeading multiplier:1 constant:10],
      [NSLayoutConstraint constraintWithItem:[self userImageView] attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:[self customContentView] attribute:NSLayoutAttributeTop multiplier:1 constant:10],
      [NSLayoutConstraint constraintWithItem:[self userImageView] attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1 constant:50],
      [NSLayoutConstraint constraintWithItem:[self userImageView] attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:[self userImageView] attribute:NSLayoutAttributeWidth multiplier:1 constant:0],
      
      [NSLayoutConstraint constraintWithItem:[self userNameLabel] attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:[self userImageView] attribute:NSLayoutAttributeTrailing multiplier:1 constant:10],
      [NSLayoutConstraint constraintWithItem:[self userNameLabel] attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:[self userImageView] attribute:NSLayoutAttributeCenterY multiplier:0.7 constant:0],
      
      [NSLayoutConstraint constraintWithItem:[self postTimeAgoLabel] attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:[self userNameLabel] attribute:NSLayoutAttributeLeading multiplier:1 constant:0],
      [NSLayoutConstraint constraintWithItem:[self postTimeAgoLabel] attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:[self userImageView] attribute:NSLayoutAttributeCenterY multiplier:1.3 constant:0],
      
      [NSLayoutConstraint constraintWithItem:[self postImageView] attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:[self customContentView] attribute:NSLayoutAttributeLeading multiplier:1 constant:0],
      [NSLayoutConstraint constraintWithItem:[self postImageView] attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:[self customContentView] attribute:NSLayoutAttributeTrailing multiplier:1 constant:0],
      [NSLayoutConstraint constraintWithItem:[self postImageView] attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:[self userImageView] attribute:NSLayoutAttributeBottom multiplier:1 constant:10],
      [NSLayoutConstraint constraintWithItem:[self postImageView] attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeHeight multiplier:1 constant:imgHeight],
      
      [NSLayoutConstraint constraintWithItem:[self postMessageLabel] attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:[self customContentView] attribute:NSLayoutAttributeLeading multiplier:1 constant:10],
      [NSLayoutConstraint constraintWithItem:[self postMessageLabel] attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:[self customContentView] attribute:NSLayoutAttributeTrailing multiplier:1 constant:-10],
      [NSLayoutConstraint constraintWithItem:[self postMessageLabel] attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:[self postImageView] attribute:NSLayoutAttributeBottom multiplier:1 constant:10],
      
      [NSLayoutConstraint constraintWithItem:[self likeCountImageView] attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:[self customContentView] attribute:NSLayoutAttributeLeading multiplier:1 constant:10],
      [NSLayoutConstraint constraintWithItem:[self likeCountImageView] attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:[self postMessageLabel] attribute:NSLayoutAttributeBottom multiplier:1 constant:10],
      [NSLayoutConstraint constraintWithItem:[self likeCountImageView] attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:[self customContentView] attribute:NSLayoutAttributeBottom multiplier:1 constant:-10],
      [NSLayoutConstraint constraintWithItem:[self likeCountImageView] attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1 constant:20],
      [NSLayoutConstraint constraintWithItem:[self likeCountImageView] attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:[self likeCountImageView] attribute:NSLayoutAttributeWidth multiplier:1 constant:0],
      
      [NSLayoutConstraint constraintWithItem:[self postCountImageView] attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:[self likeCountLabel] attribute:NSLayoutAttributeTrailing multiplier:1 constant:10],
      [NSLayoutConstraint constraintWithItem:[self postCountImageView] attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:[self likeCountImageView] attribute:NSLayoutAttributeCenterY multiplier:1 constant:0],
      [NSLayoutConstraint constraintWithItem:[self postCountImageView] attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeWidth multiplier:1 constant:20],
      [NSLayoutConstraint constraintWithItem:[self postCountImageView] attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:[self postCountImageView] attribute:NSLayoutAttributeWidth multiplier:1 constant:0],
      
      [NSLayoutConstraint constraintWithItem:[self likeCountLabel] attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:[self likeCountImageView] attribute:NSLayoutAttributeTrailing multiplier:1 constant:5],
      [NSLayoutConstraint constraintWithItem:[self likeCountLabel] attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:[self likeCountImageView] attribute:NSLayoutAttributeCenterY multiplier:1 constant:0],
      
      [NSLayoutConstraint constraintWithItem:[self postCountLabel] attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:[self postCountImageView] attribute:NSLayoutAttributeTrailing multiplier:1 constant:5],
      [NSLayoutConstraint constraintWithItem:[self postCountLabel] attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:[self likeCountLabel] attribute:NSLayoutAttributeTop multiplier:1 constant:0],
     ];
    [self addConstraints:constraints];
}

- (void)configureCellWithUserImage: (UIImage *)userImage userName: (NSString *)userName postTimeAgo: (NSString *)postTimeAgo postImage: (UIImage *)postImage postMessage: (NSString *)postMessage likeCount: (NSInteger)likeCount postCount: (NSInteger)postCount
{
    [[self userImageView] setImage:userImage];
    [[self userNameLabel] setText:userName];
    [[self postTimeAgoLabel] setText:postTimeAgo];
    [[self postImageView] setImage:postImage];
    [[self postMessageLabel] setText:postMessage];
    [[self likeCountLabel] setText:[NSString stringWithFormat:@"%ld", (long)likeCount]];
    [[self postCountLabel] setText:[NSString stringWithFormat:@"%ld", (long)postCount]];
}
@end
