//
//  PostModel.m
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/5/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "DB_PostModel.h"

@implementation DB_PostModel
@dynamic idString;
@dynamic message;
@dynamic postedAtString;
@dynamic imgUrl;
@dynamic user;

@end
