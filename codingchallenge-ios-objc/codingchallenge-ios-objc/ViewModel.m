//
//  ViewModel.m
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/7/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "ViewModel.h"

@interface ViewModel()
@property (nonatomic, assign) BOOL fetchCompleted;
@property (nonatomic, assign) BOOL imageDownloaded;
@property (nonatomic, assign) BOOL postLikeAndCommentCountFetched;
@end

@implementation ViewModel
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setupObservers];
    }
    return self;
}

- (void)setupObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationFetchCompleted:) name:kNotificationPostsFetched object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationImageDownloaded:) name:kNotificationImageDownloaded object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationPostLikeAndCommentCountFetched:) name:kNotificationPostCountAndCommentFetched object:nil];
}

- (void)fetchData
{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        [[SocialDataManager sharedManager] fetchPostsFromLocal];
        [[SocialDataManager sharedManager] fetchPostsFromServer];
    });
}

- (void)notificationFetchCompleted: (NSNotification *)noti
{
    [self setFetchCompleted:YES];
}

- (void)notificationImageDownloaded: (NSNotification *)noti
{
    [self setImageDownloaded:YES];
}

- (void)notificationPostLikeAndCommentCountFetched: (NSNotification *)noti
{
    [self setPostLikeAndCommentCountFetched:YES];
}

- (NSArray *)getPosts
{
    return [[SocialDataManager sharedManager] postModelsArray];
}

- (UIImage *)getImageFromPostModel: (PostModel *)postModel
{
    return [UIImage imageWithContentsOfFile:[postModel imagePath]];
}

- (UIImage *)getImageFromUserModel: (UserModel *)userModel
{
    return [UIImage imageWithContentsOfFile:[userModel imagePath]];
}

- (NSString *)convertToStringAgoFromDateString: (NSString *)dateString
{
    NSDate *date = [[SocialDataManager sharedManager] convertToDateFromString:dateString];
    CGFloat seconds = fabs([date timeIntervalSinceNow]);
    if (seconds < 60) {
        return [NSString stringWithFormat:@"%@ sec ago", @((int)seconds)];
    }
    
    CGFloat minutes = seconds / 60;
    if (minutes < 60) {
        return [NSString stringWithFormat:@"%@ min ago", @((int)minutes)];
    }
    
    CGFloat hours = minutes / 60;
    if (hours < 24) {
        return [NSString stringWithFormat:@"%@ hrs ago", @((int)hours)];
    }
    
    CGFloat days = hours / 24;
    if (days < 365) {
        return [NSString stringWithFormat:@"%@ days ago", @((int)days)];
    }
    
    CGFloat years = days / 365;
    return [NSString stringWithFormat:@"%@ yrs ago", @((int)years)];
    
}
@end
