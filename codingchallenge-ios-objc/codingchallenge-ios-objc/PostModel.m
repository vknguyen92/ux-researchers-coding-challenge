//
//  PostModel.m
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/6/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "PostModel.h"
#import "UserModel.h"
#import "DataPathManager.h"

@implementation PostModel

- (instancetype)initWithIdString: (NSString *)idString message: (NSString *)message postedAtString: (NSString *)postedAtString imgUrl: (NSString *)imgUrl likeCount: (NSNumber *)likeCount commentCount: (NSNumber *)commentCount user: (UserModel *)user
{
    self = [super init];
    if (self) {
        _idString = idString;
        _message = message;
        _postedAtString = postedAtString;
        _imgUrl = imgUrl;
        _user = user;
        _likeCount = likeCount;
        _commentCount = commentCount;
    }
    return self;
}

+ (PostModel *)addPostWithIdString: (NSString *)idString message: (NSString *)message postedAtString: (NSString *)postedAtString imgUrl: (NSString *)imgUrl likeCount: (NSNumber *)likeCount commentCount: (NSNumber *)commentCount user: (UserModel *)user
{
    PostModel *postModel = [[PostModel alloc] initWithIdString:idString message:message postedAtString:postedAtString imgUrl:imgUrl likeCount:likeCount commentCount:commentCount user:user];
    return postModel;
}

+ (PostModel *)fromDbPost: (DB_PostModel *)dbPost
{
    PostModel *postModel = [[PostModel alloc] initWithIdString:[dbPost idString] message:[dbPost message] postedAtString:[dbPost postedAtString] imgUrl:[dbPost imgUrl] likeCount: [dbPost likeCount] commentCount: [dbPost commentCount] user:[UserModel fromDbUser:[dbPost user]]];
    return postModel;
}

- (NSString *)imagePath
{
    NSString *escapedUrlString = [[self imgUrl] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *imageName = [NSString stringWithFormat:@"%@_%@", [self idString], escapedUrlString];
    return [[DataPathManager postImageFolder] stringByAppendingPathComponent:imageName];
}
@end
