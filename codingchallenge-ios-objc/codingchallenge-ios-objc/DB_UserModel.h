//
//  UserModel.h
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/5/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface DB_UserModel : NSManagedObject
@property (nonatomic, retain) NSString *idString;
@property (nonatomic, retain) NSString *fullName;
@property (nonatomic, retain) NSString *phoneNumber;
@property (nonatomic, retain) NSString *dobString;
@property (nonatomic, retain) NSString *imgUrl;
@end
