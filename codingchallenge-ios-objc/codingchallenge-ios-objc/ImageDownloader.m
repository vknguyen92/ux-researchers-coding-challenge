//
//  ImageDownloader.m
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/6/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "ImageDownloader.h"
#import "ImageDownloadOperation.h"

@interface ImageDownloader()
@property (nonatomic, strong) NSOperationQueue *imageDownloadQueue;
@end

@implementation ImageDownloader
+ (id)sharedManager {
    static ImageDownloader *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
        [sharedManager setImageDownloadQueue:[[NSOperationQueue alloc] init]];
        [[sharedManager imageDownloadQueue] setMaxConcurrentOperationCount:3];
    });
    return sharedManager;
}

- (void)downloadImageFromUrl: (NSString *)url saveToPath: (NSString *)imagePath completion: (void (^)())completion
{
    ImageDownloadOperation *imageDownloader = [[ImageDownloadOperation alloc] initWithUrl:url progressBlock:^(CGFloat progress) {
        //
    } completionBlock:^(ImageDownloadOperation *ImageDownloadOperation) {
        [self saveImageToLocalFileWithPath:imagePath fromTempFilePath:[ImageDownloadOperation tempDownloadedFileLocation]];
        completion();
    }];
    
    NSBlockOperation *operation = [[NSBlockOperation alloc] init];
    [operation addExecutionBlock:^{
        [imageDownloader startDownload];
    }];
    
    [[self imageDownloadQueue] addOperation:operation];
}

- (void)saveImageToLocalFileWithPath: (NSString *)imagePath fromTempFilePath: (NSURL *)tempFilePath
{
    NSError *error;
    [[NSFileManager defaultManager] copyItemAtPath:[tempFilePath relativePath] toPath:imagePath error:&error];
}

@end
