//
//  UserModel.m
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/5/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "DB_UserModel.h"

@implementation DB_UserModel
@dynamic idString;
@dynamic fullName;
@dynamic phoneNumber;
@dynamic dobString;
@dynamic imgUrl;

@end
