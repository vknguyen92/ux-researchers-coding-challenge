//
//  ApiClient.m
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/6/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "ApiClient.h"

@interface ApiClient()
@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSURL *baseUrl;
@end

@implementation ApiClient

+ (id)sharedManager {
    static ApiClient *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
        
        if ([sharedManager session] == nil) {
            NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
            [sharedManager setSession:[NSURLSession sessionWithConfiguration:configuration]];
        }
        
        NSString *URLString = @"http://thedemoapp.herokuapp.com";
        NSURL *url = [NSURL URLWithString:URLString];
        [sharedManager setBaseUrl:url];
    });
    return sharedManager;
}

- (void)getPostsCompletion: (void (^)(NSData *data, NSError *error))completion
{
    NSURL *url = [[self baseUrl] URLByAppendingPathComponent:@"post"];
    [self getDataFromUrl:url completion:completion];
}

- (void)getLikeCountFromPostId: (NSString *)postId completion: (void (^)(NSData *data, NSError *error))completion
{
    NSURL *url = [[self baseUrl] URLByAppendingPathComponent:[NSString stringWithFormat:@"post/%@/likeCount", postId]];
    [self getDataFromUrl:url completion:completion];
}

- (void)getCommentCountFromPostId: (NSString *)postId completion: (void (^)(NSData *data, NSError *error))completion
{
    NSURL *url = [[self baseUrl] URLByAppendingPathComponent:[NSString stringWithFormat:@"post/%@/commentCount", postId]];
    [self getDataFromUrl:url completion:completion];
}

- (void)getDataFromUrl: (NSURL *)url completion: (void (^)(NSData *data, NSError *error))completion
{
    NSLog(@"URL: %@", [url absoluteString]);
    NSURLSessionDataTask *dataTask = [[self session] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        completion(data, error);
    }];
    
    [dataTask resume];
}

@end
