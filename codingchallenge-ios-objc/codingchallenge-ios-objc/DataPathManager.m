//
//  DataPathManager.m
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/6/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "DataPathManager.h"

@implementation DataPathManager
+ (NSString *)socialFolder
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : NSTemporaryDirectory();
    return [basePath stringByAppendingPathComponent:@"Social"];
}

+ (NSString *)postImageFolder
{
    return [[self socialFolder] stringByAppendingPathComponent:@"PostImage"];
}

+ (NSString *)userImageFolder
{
    return [[self socialFolder] stringByAppendingPathComponent:@"UserImage"];
}

@end
