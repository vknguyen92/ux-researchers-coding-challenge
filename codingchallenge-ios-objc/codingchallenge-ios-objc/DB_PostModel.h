//
//  PostModel.h
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/5/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DB_UserModel.h"
#import <CoreData/CoreData.h>

@interface DB_PostModel : NSManagedObject
@property (nonatomic, retain) NSString *idString;
@property (nonatomic, retain) NSString *message;
@property (nonatomic, retain) NSString *postedAtString;
@property (nonatomic, retain) NSString *imgUrl;
@property (nonatomic, retain) NSNumber *likeCount;
@property (nonatomic, retain) NSNumber *commentCount;
@property (nonatomic, retain) DB_UserModel *user;
@end
