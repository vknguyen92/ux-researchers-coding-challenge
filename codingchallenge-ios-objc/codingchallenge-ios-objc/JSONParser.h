//
//  JSONParse.h
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/6/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONParser : NSObject
+ (id)sharedManager;
- (NSArray *)parsePostJsonFromData: (NSData *)data;
- (NSInteger)parseCountFromData: (NSData *)data;
@end
