//
//  JSONParse.m
//  codingchallenge-ios-objc
//
//  Created by Nguyen Vu on 5/6/17.
//  Copyright © 2017 User Experience Research. All rights reserved.
//

#import "JSONParser.h"
#import "PostModel.h"
#import "UserModel.h"

@implementation JSONParser
+ (id)sharedManager {
    static JSONParser *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

- (NSArray *)parsePostJsonFromData: (NSData *)data
{
    NSError *error = nil;
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    NSLog(@"Post Json:\n%@", jsonDict);
    NSNumber *code = [jsonDict objectForKey:@"code"];
    if ((code != nil) && (code.integerValue == 200)) {
        NSMutableArray *postModelsArray = [[NSMutableArray alloc] init];
        
        NSArray *postsArray = [jsonDict objectForKey:@"data"];
        for (id post in postsArray) {
            NSDictionary *postDict = (NSDictionary *)post;
            if (postDict != nil) {
                NSString *idString = [[postDict objectForKey:@"_id"] stringValue];
                NSString *message = [postDict objectForKey:@"message"];
                NSString *postedAtString = [postDict objectForKey:@"postedAt"];
                NSString *imageURL = [postDict objectForKey:@"imageURL"];
                
                UserModel *userModel = [self parseUserFromParentJsonDict:postDict];
                if (userModel != nil) {
                    if (idString != nil) {
                        PostModel *postModel = [PostModel addPostWithIdString:idString message:message postedAtString:postedAtString imgUrl:imageURL likeCount:0 commentCount:0 user:userModel];
                        [postModelsArray addObject:postModel];
                    }
                }
            }
        }
        return postModelsArray;
    }
    return nil;
}

- (UserModel *)parseUserFromParentJsonDict: (NSDictionary *)postDict
{
    NSDictionary *userDict = [postDict objectForKey:@"user"];
    NSString *idString = [[userDict objectForKey:@"_id"] stringValue];
    NSString *fullName = [userDict objectForKey:@"fullName"];
    NSString *mobileNo = [userDict objectForKey:@"mobileNo"];
    NSString *dobString = [userDict objectForKey:@"dob"];
    NSString *imageURL = [userDict objectForKey:@"imageURL"];
    if (idString != nil) {
        UserModel *userModel = [UserModel addUserWithIdString:idString fullName:fullName phoneNumber:mobileNo dobString:dobString imgUrl:imageURL];
        return userModel;
    }
    return nil;
}

- (NSInteger)parseCountFromData: (NSData *)data
{
    NSError *error = nil;
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    NSNumber *code = [jsonDict objectForKey:@"code"];
    if ((code != nil) && (code.integerValue == 200)) {
        NSNumber *likeCount = [jsonDict objectForKey:@"data"];
        return [likeCount integerValue];
    }
    return 0;
}
@end
